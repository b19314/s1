package com.zuitt.batch193;

public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello World");

    }

//    public = Access Modifiers = Who can see this method
//    static = Non-access Modifier = How should this method behave?
    //    static keyword field exists accross all the class instances
//    Void = Return Type - What should this method return?
    //    void keyword specify that a method doesn't return anything
//    Main = Method name -

}