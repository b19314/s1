
package com.zuitt.batch193;

import jdk.swing.interop.SwingInterOpUtils;

public class Variables {
    public static void main(String[] args){
//  variable declaration
        int myNum;

//  variable initialization
        myNum = 29; //Literals = any constant value which can be assigned to the variable

        System.out.println(myNum);

//  Reassignment
        myNum = 1;
        System.out.println(myNum);

//  variabbles in java are usually called INDENTIFIERS
        int age;
        char middle_name;

//  CONSTANT
//  Java Constants should be all UPPERCASE
        final int PRINCIPAL = 1000;
        final String LAST_NAME = "Doe";

//  PRIMITIVE DATA TYPES
        char letter = 'A'; //single qoute for a single character
        boolean isMarried = false;
//  Whole numbers
        byte students = 127;
        short seats = 32767;
        int localPopulation = 2_146_273_827; // _ underscore is not read as a value but for us to read the code

        System.out.println(localPopulation);

        long worldPopulation = 2_146_273_827L; // L meaning Long literal

//  Decimal points
        float price = 12.99F; // F = float
        double temperature = 1345.43254;
//  to grab data type of a variable, we will use getClass() with    (Object) to convert it to object
        System.out.println(((Object)temperature).getClass());

//  NON-PRIMITIVE DATA TYPES / REFERENCING DATA TYPES
//      String
        String name = "John Doe";
        System.out.println(name);
//      String can access to method to manipulate a data
        String editedName = name.toLowerCase();
        System.out.println(editedName);
        System.out.println(name.getClass()); //no need to convert to object to get the data type

//      escape character
        System.out.println("c:\\windows\\desktop"); // \". \n

//      explicit casting
        int num1 =5;
        double num2 = 2.7;
//        int anotherTotal = num1 + num2;
        double total = (double) (num1 + num2);
        System.out.println(total);

//      both strings
        String mathGrade = "90";
        String englishGrade = "85";
        System.out.println(mathGrade + englishGrade);

//      Converting strings to integers
        int totalGrade = Integer.parseInt(mathGrade) + Integer.parseInt(englishGrade);
        System.out.println(totalGrade);

//      Convert again into string
        String stringGrade = Integer.toString(totalGrade);
        System.out.println(stringGrade.getClass());

    }
}
