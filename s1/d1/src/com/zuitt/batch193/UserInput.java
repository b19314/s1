package com.zuitt.batch193;

import java.util.Scanner;

public class UserInput {
    public static void main(String[] args){
//  Scanner class is use to get user input and it is important from java.utility package
//  Scanner is an object
        Scanner appScanner = new Scanner(System.in);
        System.out.println("What's your name?");
        String myName = appScanner.nextLine().trim();
//  nextLine() will always convert the value into an object
//  trim() removing white spaces before andn after the words
        System.out.println("Username is: " + myName);
        System.out.println("What's your age?");
        int myAge = appScanner.nextInt();
        System.out.println("Age of the user is: " + myAge);

        System.out.println("What is your weight?");
        double weight = appScanner.nextDouble();
        System.out.println("The weight of the user is: " +weight);
    }
}