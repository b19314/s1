package com.zuitt.batch193;

import java.util.Scanner;

public class a1 {
    public static void main(String[] args){
        Scanner appScanner = new Scanner(System.in);
        System.out.println("First Name:");
        String myFirstName = appScanner.nextLine().trim();
        System.out.println("Last Name:");
        String myLastName = appScanner.nextLine().trim();

        System.out.println("First Subject Grade:");
        double firstSubject = appScanner.nextDouble();
        System.out.println("Second Subject Grade:");
        double secondSubject = appScanner.nextDouble();
        System.out.println("Third Subject Grade:");
        double thirdSubject = appScanner.nextDouble();

        double totalGrade = (double) (firstSubject + secondSubject + thirdSubject);

        System.out.println("Good day, " + myFirstName  + " " + myLastName + ".");

        System.out.println("Your grade average is: " + totalGrade/ 3);


    }
}
